package Tehdas;

import Tuotteet.AdidasFarmarit;
import Tuotteet.AdidasKengät;
import Tuotteet.AdidasLippis;
import Tuotteet.AdidasTpaita;
import Tuotteet.Farmarit;
import Tuotteet.Kengät;
import Tuotteet.Lippis;
import Tuotteet.Tpaita;

public class AdidasTehdas implements vaateTehdas {

	@Override
	public Farmarit pueFarmarit() {
		return new AdidasFarmarit();
	}

	@Override
	public Tpaita pueTpaita() {
		return new AdidasTpaita();
	}

	@Override
	public Lippis pueLippis() {
		return new AdidasLippis();
	}

	@Override
	public Kengät pueKengät() {
		return new AdidasKengät();
	}

}
