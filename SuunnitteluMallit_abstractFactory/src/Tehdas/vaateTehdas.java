package Tehdas;

import Tuotteet.Farmarit;
import Tuotteet.Kengät;
import Tuotteet.Lippis;
import Tuotteet.Tpaita;

public interface vaateTehdas {
	
	Farmarit pueFarmarit();
	Tpaita pueTpaita();
	Lippis pueLippis();
	Kengät pueKengät();

}
