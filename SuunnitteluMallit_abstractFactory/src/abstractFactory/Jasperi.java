package abstractFactory;

import Tehdas.vaateTehdas;
import Tuotteet.Farmarit;
import Tuotteet.Kengät;
import Tuotteet.Lippis;
import Tuotteet.Tpaita;

public class Jasperi {
	
	public static String pukeudu(vaateTehdas asu) {
		Farmarit farmarit = asu.pueFarmarit();
		Tpaita tpaita = asu.pueTpaita();
		Kengät kengät = asu.pueKengät();
		Lippis lippis = asu.pueLippis();
		return "Jasperin asukokonaisuuteen kuuluu: \n\t" + farmarit + "\n\t"+ tpaita +"\n\t"+ kengät +"\n\t"+ lippis;
	}

}
