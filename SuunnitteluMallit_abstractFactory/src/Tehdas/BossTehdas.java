package Tehdas;

import Tuotteet.BossFarmarit;
import Tuotteet.BossKengät;
import Tuotteet.BossLippis;
import Tuotteet.BossTpaita;
import Tuotteet.Farmarit;
import Tuotteet.Kengät;
import Tuotteet.Lippis;
import Tuotteet.Tpaita;

public class BossTehdas implements vaateTehdas {

	@Override
	public Farmarit pueFarmarit() {
		return new BossFarmarit();
	}

	@Override
	public Tpaita pueTpaita() {
		return new BossTpaita();
	}

	@Override
	public Lippis pueLippis() {
		return new BossLippis();
	}

	@Override
	public Kengät pueKengät() {
		return new BossKengät();
	}

}
